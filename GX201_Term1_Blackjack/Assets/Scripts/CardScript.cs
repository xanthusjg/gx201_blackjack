using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardScript : MonoBehaviour {
    public int cardValue;

    // method to get value of a card
    public int ReturnCardValue () {

        return cardValue;
    }

    //method to set value of a card
    public int SetCardValue (int newValue) {
        cardValue = newValue;

        return cardValue;
    }

    //disable card
    public void ResetCard () {
        gameObject.SetActive (false);
    }
    //enable card
    public void ShowCard () {
        gameObject.SetActive (true);
    }
}