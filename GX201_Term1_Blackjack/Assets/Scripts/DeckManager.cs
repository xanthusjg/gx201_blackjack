﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class DeckManager : MonoBehaviour {
    [SerializeField] GameObject[] cards;
    [SerializeField] GameObject cardSpawnParent, gameManager;
    [SerializeField] List<GameObject> cardList;
    [SerializeField] GameObject playerCard1, playerCard2, playerCard3, playerCard4, playerCard5, playerCard6, dealerCard1, dealerCard2, dealerCard3, dealerCard4;
    Vector3 playerCard1Pos, playerCard2Pos, playerCard3Pos, playerCard4Pos, playerCard5Pos, playerCard6Pos, dealerCard1Pos, dealerCard2Pos, dealerCard3Pos, dealerCard4Pos;
    private bool stayIsFinished = false;
    private bool haveDealt = false;
    bool card3IsDealt;
    bool card4IsDealt = false;
    bool card5IsDealt = false;
    bool card6IsDealt = false;
    bool dCard2IsDealt = false;
    bool dCard3IsDealt = false;
    bool dCard4IsDealt = false;

    void Start () {
        //set all card spawn points
        playerCard1Pos = playerCard1.transform.position;
        playerCard2Pos = playerCard2.transform.position;
        playerCard3Pos = playerCard3.transform.position;
        playerCard4Pos = playerCard4.transform.position;
        playerCard5Pos = playerCard5.transform.position;
        playerCard6Pos = playerCard6.transform.position;

        dealerCard1Pos = dealerCard1.transform.position;
        dealerCard2Pos = dealerCard2.transform.position;
        dealerCard3Pos = dealerCard3.transform.position;
        dealerCard4Pos = dealerCard4.transform.position;

        

    }
    private void OnEnable () {
        //set  stored values to 0
        PlayerPrefs.SetInt ("pCard1Value", 0);
        PlayerPrefs.SetInt ("pCard2Value", 0);
        PlayerPrefs.SetInt ("pCard3Value", 0);
        PlayerPrefs.SetInt ("pCard4Value", 0);
        PlayerPrefs.SetInt ("pCard5Value", 0);
        PlayerPrefs.SetInt ("pCard6Value", 0);

        PlayerPrefs.SetInt ("dCard1Value", 0);
        PlayerPrefs.SetInt ("dCard2Value", 0);
        PlayerPrefs.SetInt ("dCard3Value", 0);
        PlayerPrefs.SetInt ("dCard4Value", 0);

        ShuffleDeck ();

    }

    //assign cards to random index in list
    public void ShuffleDeck () {
        for (var i = 0; i < cards.Length; i++) {
            int j = Random.Range (i, cards.Length);
            GameObject temp = cardList[i];
            cardList[i] = cardList[j];
            cardList[j] = temp;
        }
    }

    //deal initial hand and store card values in playerprefs
    // ante up when game starts
    public void InitialDeal () {

        if (!haveDealt) {
            GameObject clone1 = Instantiate (cardList[0], playerCard1Pos, Quaternion.identity);
            clone1.SetActive (true);
            AssignNewParent (clone1, cardSpawnParent);
            PlayerPrefs.SetInt ("pCard1Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());

            ShortenArray (0);

            GameObject clone2 = Instantiate (cardList[0], playerCard2Pos, Quaternion.identity);
            clone2.SetActive (true);
            AssignNewParent (clone2, cardSpawnParent);
            PlayerPrefs.SetInt ("pCard2Value", clone2.transform.GetComponent<CardScript> ().ReturnCardValue ());

            ShortenArray (0);

            GameObject clone3 = Instantiate (cardList[0], dealerCard1Pos, Quaternion.identity);
            clone3.SetActive (true);
            PlayerPrefs.SetInt ("dCard1Value", clone3.transform.GetComponent<CardScript> ().ReturnCardValue ());

            ShortenArray (0);

            gameManager.transform.GetComponent<GameManager> ().Ante ();

            haveDealt = true;
        } else if (haveDealt) {
            Debug.Log ("cards have already been dealt");
        }

    }

    //removes 1 card from list (specified index in overload)
    public void ShortenArray (int indexToRemove) {

        cardList.RemoveAt (indexToRemove);

    }

    // Deal more cards to player if it is possible
    // Each card is drawn from the top of the list (deck) and is removed from the list
    public void Hit () {

        if (haveDealt && !stayIsFinished) {
            if (PlayerPrefs.GetInt ("PlayerHandTotal") < 21 && card6IsDealt == false && card5IsDealt == true && card4IsDealt == true && card3IsDealt == true) {

                GameObject clone1 = Instantiate (cardList[0], playerCard6Pos, Quaternion.identity);
                clone1.SetActive (true);
                AssignNewParent (clone1, cardSpawnParent);
                PlayerPrefs.SetInt ("pCard6Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());
                card6IsDealt = true;

                ShortenArray (0);
            }
            if (PlayerPrefs.GetInt ("PlayerHandTotal") < 21 && card5IsDealt == false && card4IsDealt == true && card3IsDealt == true) {

                GameObject clone1 = Instantiate (cardList[0], playerCard5Pos, Quaternion.identity);
                clone1.SetActive (true);
                AssignNewParent (clone1, cardSpawnParent);
                PlayerPrefs.SetInt ("pCard5Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());
                card5IsDealt = true;

                ShortenArray (0);
            }
            if (PlayerPrefs.GetInt ("PlayerHandTotal") < 21 && card4IsDealt == false && card3IsDealt == true) {

                GameObject clone1 = Instantiate (cardList[0], playerCard4Pos, Quaternion.identity);
                clone1.SetActive (true);
                AssignNewParent (clone1, cardSpawnParent);
                PlayerPrefs.SetInt ("pCard4Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());
                card4IsDealt = true;

                ShortenArray (0);
            }

            if (PlayerPrefs.GetInt ("PlayerHandTotal") < 21 && card3IsDealt == false) {

                GameObject clone1 = Instantiate (cardList[0], playerCard3Pos, Quaternion.identity);
                clone1.SetActive (true);
                AssignNewParent (clone1, cardSpawnParent);
                PlayerPrefs.SetInt ("pCard3Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());
                card3IsDealt = true;

                ShortenArray (0);

            }

            if (PlayerPrefs.GetInt ("PlayerHandTotal") == 21) {
                Debug.Log ("You win the round");

            } else if (PlayerPrefs.GetInt ("PlayerHandTotal") > 21) {
                Debug.Log ("you are bust");
            }
        }
    }

    //starts dealer's turn - dealer will draw cards until hand value reaches minimum of 16 
    public void PlayerStay () {
        if (haveDealt) {

            StartCoroutine (DealerDraw (1f));
            stayIsFinished = true;

        }
    }

    // make the dealer draw until it has a minimum of 16
    // dealer will always set value of an ace to 11
    // dealer will pause for a specified amount of time between drawing cards
    private IEnumerator DealerDraw (float waitTime) {

        for (var i = 0; i < 3; i++) {
            yield return new WaitForSeconds (waitTime);

            if (PlayerPrefs.GetInt ("DealerHandTotal") < 16 && dCard4IsDealt == false && dCard3IsDealt == true && dCard2IsDealt == true) {
                GameObject clone1 = Instantiate (cardList[0], dealerCard4Pos, Quaternion.identity);
                clone1.SetActive (true);

                if (clone1.transform.GetComponent<CardScript> ().ReturnCardValue () == 1) {
                    AceValueSet (clone1);
                }

                PlayerPrefs.SetInt ("dCard4Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());

                ShortenArray (0);
                dCard4IsDealt = true;
            }

            if (PlayerPrefs.GetInt ("DealerHandTotal") < 16 && dCard3IsDealt == false && dCard2IsDealt == true) {

                GameObject clone1 = Instantiate (cardList[0], dealerCard3Pos, Quaternion.identity);
                clone1.SetActive (true);

                if (clone1.transform.GetComponent<CardScript> ().ReturnCardValue () == 1) {
                    AceValueSet (clone1);
                }

                PlayerPrefs.SetInt ("dCard3Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());

                ShortenArray (0);
                dCard3IsDealt = true;
            }

            if (PlayerPrefs.GetInt ("DealerHandTotal") < 16 && dCard2IsDealt == false) {

                GameObject clone1 = Instantiate (cardList[0], dealerCard2Pos, Quaternion.identity);
                clone1.SetActive (true);

                if (clone1.transform.GetComponent<CardScript> ().ReturnCardValue () == 1) {
                    AceValueSet (clone1);
                }

                PlayerPrefs.SetInt ("dCard2Value", clone1.transform.GetComponent<CardScript> ().ReturnCardValue ());

                ShortenArray (0);
                dCard2IsDealt = true;
            }

        }

    }

    //select ace with raycast and change its value
    //method will find specific instance of card and store its value in the correct PlayerPrefs variable
    public void SelectAce (InputAction.CallbackContext context) {
        if (context.performed) {
            var ray = Camera.main.ScreenPointToRay (Mouse.current.position.ReadValue ());
            RaycastHit hit;
            if (Physics.Raycast (ray, out hit)) {
                GameObject selection = hit.collider.gameObject;
                Debug.Log ("card " + selection.gameObject.name + " is selected");

                //is selected object is an ace, change its value
                if (selection.transform.GetComponent<CardScript> ().ReturnCardValue () == 1) {
                    selection.transform.GetComponent<CardScript> ().SetCardValue (11);
                    Debug.Log ("Ace value is now: " + selection.transform.GetComponent<CardScript> ().ReturnCardValue ());

                    //save value change to correct card
                    if (selection.transform.position == playerCard1Pos) {
                        PlayerPrefs.SetInt ("pCard1Value", selection.transform.GetComponent<CardScript> ().ReturnCardValue ());
                    }
                    if (selection.transform.position == playerCard2Pos) {
                        PlayerPrefs.SetInt ("pCard2Value", selection.transform.GetComponent<CardScript> ().ReturnCardValue ());
                    }
                    if (selection.transform.position == playerCard3Pos) {
                        PlayerPrefs.SetInt ("pCard3Value", selection.transform.GetComponent<CardScript> ().ReturnCardValue ());
                    }
                    if (selection.transform.position == playerCard4Pos) {
                        PlayerPrefs.SetInt ("pCard4Value", selection.transform.GetComponent<CardScript> ().ReturnCardValue ());
                    }
                    if (selection.transform.position == playerCard5Pos) {
                        PlayerPrefs.SetInt ("pCard5Value", selection.transform.GetComponent<CardScript> ().ReturnCardValue ());
                    }
                    if (selection.transform.position == playerCard6Pos) {
                        PlayerPrefs.SetInt ("pCard6Value", selection.transform.GetComponent<CardScript> ().ReturnCardValue ());
                    } else {
                        //Do Nothing
                    }
                } else if (selection.gameObject.CompareTag ("Table")) {
                    //Do Nothing
                }
            }

        }
    }

    //change value of Ace
    public void AceValueSet (GameObject cardToChange) {
        cardToChange.transform.GetComponent<CardScript> ().SetCardValue (11);
    }

    //assigns new parent object to specified object
    public void AssignNewParent (GameObject spawnedCard, GameObject newParent) {
        spawnedCard.transform.parent = newParent.transform;
    }

    //returns the bool stayIsFinished so that it can be used in other classes
    public bool StayIsFinished () {

        return stayIsFinished;
    }
}