﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    //quit game
    public void ExitGame () {
        Application.Quit ();
    }

    //restarts game, but keeps all playerprefs
    public void ReloadScene () {
        SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
    }

    //resarts game and deletes all playerprefs
    public void ResetGame () {

        PlayerPrefs.DeleteAll ();

        SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
    }
}