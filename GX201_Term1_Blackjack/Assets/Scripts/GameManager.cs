﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [SerializeField] GameObject nextRoundButton, deckManager;
    [SerializeField] Text gameStatusText, currentScore, currentMoneyText, totalBetText;
    [SerializeField] float defaultMoney = 1000f, defaultBet = 30f, defaultMultiplier = 2.5f;
    float totalBet = 0f;
    float currentMoney, currentBet;
    bool paidOut = false;

    //setup text refrences
    void Start () {
        gameStatusText = gameStatusText.GetComponent<Text> ();
        currentScore = currentScore.GetComponent<Text> ();
        currentMoneyText = currentMoneyText.GetComponent<Text> ();
        totalBetText = totalBetText.GetComponent<Text> ();

    }

    //set money to default value if playerprefs has not been set
    private void OnEnable () {
        currentBet = defaultBet;

        if (PlayerPrefs.GetFloat ("TotalMoney") == 0.0f) {
            StoreTotalMoney (defaultMoney);
            currentMoney = PlayerPrefs.GetFloat ("TotalMoney");
        } else {
            currentMoney = PlayerPrefs.GetFloat ("TotalMoney");
        }

    }

    //update game win/lose status
    void Update () {
        currentScore.text = "Current card total: " + PlayerPrefs.GetInt ("PlayerHandTotal");
        currentMoneyText.text = "$ " + currentMoney;
        totalBetText.text = "Betting: $ " + totalBet;

        InstantWinCheck ();

        InstantLossCheck ();

        StartCoroutine (WinCheck ());

        StartCoroutine (DealerWinCheck ());

    }

    //Check if player has a total of 21 at any point
    public void InstantWinCheck () {
        if (PlayerPrefs.GetInt ("PlayerHandTotal") == 21) {
            nextRoundButton.SetActive (true);

            gameStatusText.text = "Round Won";
            //only pay out once
            if (paidOut == false) {
                IncreaseMoneyOnWin ();
                paidOut = true;
            }

        }
    }

    //check if player has bust
    public void InstantLossCheck () {
        if (PlayerPrefs.GetInt ("PlayerHandTotal") > 21) {
            nextRoundButton.SetActive (true);

            gameStatusText.text = "Round Lost";
            //only pay out once
            if (paidOut == false) {
                DecreaseMoneyOnLoss ();
                paidOut = true;
            }

        }
    }

    //Check if player has higher value than dealer at the end of the game
    public IEnumerator WinCheck () {
        if (deckManager.transform.GetComponent<DeckManager> ().StayIsFinished () == true && PlayerPrefs.GetInt ("PlayerHandTotal") > PlayerPrefs.GetInt ("DealerHandTotal") && PlayerPrefs.GetInt ("PlayerHandTotal") <= 21) {
            yield return new WaitForSeconds (4f);
            nextRoundButton.SetActive (true);

            gameStatusText.text = "Round Won";

            //only pay out once
            if (paidOut == false) {
                IncreaseMoneyOnWin ();
                paidOut = true;
            }
        }

        if (PlayerPrefs.GetInt ("DealerHandTotal") > 21) {
            yield return new WaitForSeconds (0f);
            nextRoundButton.SetActive (true);

            gameStatusText.text = "Round Won";

            //only pay out once
            if (paidOut == false) {
                IncreaseMoneyOnWin ();
                paidOut = true;
            }
        }
    }

    //Check if the dealer has higher value than the player at the end of the game
    public IEnumerator DealerWinCheck () {
        if (deckManager.transform.GetComponent<DeckManager> ().StayIsFinished () == true && PlayerPrefs.GetInt ("PlayerHandTotal") < PlayerPrefs.GetInt ("DealerHandTotal") && PlayerPrefs.GetInt ("DealerHandTotal") <= 21) {
            yield return new WaitForSeconds (3f);
            nextRoundButton.SetActive (true);

            gameStatusText.text = "Round Lost";

            //only pay out once
            if (paidOut == false) {
                DecreaseMoneyOnLoss ();
                paidOut = true;
            }
        }
    }

    public void IncreaseMoneyOnWin () {
        currentMoney = currentMoney + totalBet * defaultMultiplier;
        StoreTotalMoney (currentMoney);
    }

    public void DecreaseMoneyOnLoss () {
        totalBet = 0f;
    }

    public void Ante () {
        currentMoney = currentMoney - defaultBet;
        totalBet = totalBet + defaultBet;
        StoreTotalMoney (currentMoney);
    }

    public void Bet () {
        currentMoney = currentMoney - currentBet;

        totalBet = totalBet + currentBet;
        StoreTotalMoney (currentMoney);
    }

    public void StoreTotalMoney (float valueToStore) {
        PlayerPrefs.SetFloat ("TotalMoney", valueToStore);
    }
}