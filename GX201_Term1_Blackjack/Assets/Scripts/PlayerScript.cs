﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {


    int playerHandTotal = 0;
    int dealerHandTotal = 0;

    //calculate hand totals each frame 
    private void Update () {
        CalcPlayerHandTotal ();
        // Debug.Log("player hand total is" + PlayerHandTotal);
        CalcDealerHandTotal ();
    }

    //calculate totals from values stored in PlayerPrefs
    public void CalcPlayerHandTotal () {
        playerHandTotal = PlayerPrefs.GetInt ("pCard1Value") + PlayerPrefs.GetInt ("pCard2Value") + PlayerPrefs.GetInt ("pCard3Value") + PlayerPrefs.GetInt ("pCard4Value") + PlayerPrefs.GetInt ("pCard5Value") + PlayerPrefs.GetInt ("pCard6Value");
        PlayerPrefs.SetInt ("PlayerHandTotal", playerHandTotal);
    }
    //calculate totals from values stored in PlayerPrefs
    public void CalcDealerHandTotal () {
        dealerHandTotal = PlayerPrefs.GetInt ("dCard1Value") + PlayerPrefs.GetInt ("dCard2Value") + PlayerPrefs.GetInt ("dCard3Value") + PlayerPrefs.GetInt ("dCard4Value");
        PlayerPrefs.SetInt ("DealerHandTotal", dealerHandTotal);
    }
}